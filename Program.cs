﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GGBot
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Here, you need to pass a guid or any other unique string.
            if (!SingleInstance.Start("0f03c714-b597-4c17-a351-62f35535599a"))
            {
                MessageBox.Show("Application is already running.");
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            // Mark single instance as closed
            SingleInstance.Stop();
        }
    }
}
