﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Management;
using Microsoft.Win32;

namespace GGBot
{
    public partial class Form1 : Form
    {
        //Process geombotProcess;
        public enum GeombotState { Connected, Disconnected }
        public static GeombotState geombotState;
        public static SerialManager serialManager;
        public static string ggbotPort;
        HttpServer ggbotServer;
        Thread serverThread;

        public Form1()
        {
            InitializeComponent();
            geombotState = GeombotState.Disconnected;

            // The server starts when the application is started
            serverThread = new Thread(()=> { ggbotServer = new HttpServer("http://127.0.0.1:3002/"); });
            serverThread.Start();

            // The connection with the serial port will be established when the user clicks the connect button
            serialManager = new SerialManager();
            ggbotPort = serialManager.AutodetectArduinoPort();

            //Ports List Refresh
            RefreshPortList();

            // Auto select first result
            //if (PortsList.Items.Count > 0)
            //    PortsList.SelectedIndex = 0;
        }



        //#####################################################
        //GEOMBOT COMMUNICATION
        //#####################################################

        private void Connect()
        {
            StartSerialCommunication();
            Connect_Btn.BackColor = Color.LightGreen;
            Connect_Btn.Text = "Connesso";
            geombotState = GeombotState.Connected;
        }

        private void Disconnect()
        {
            CloseSerialCommunication();
            Connect_Btn.BackColor = Color.MistyRose;
            Connect_Btn.Text = "Connetti GGbot";
            geombotState = GeombotState.Disconnected;
        }

        private void CloseApplication(object sender, EventArgs e)
        {
            if (geombotState == GeombotState.Connected)
                Disconnect();

            notifyIcon1.Dispose();
            serverThread.Abort();
            Application.Exit();
        }

        private void StartSerialCommunication()
        {
            serialManager.Connect(ggbotPort);
        }

        private void CloseSerialCommunication()
        {
            serialManager.Disconnect();
        }

        private void RefreshPortList()
        {
            // Update available ports list
            string[] availablePorts = serialManager.GetPorts();
            string[] portListItems = PortsList.Items.Cast<string>().ToArray();
            foreach (string port in availablePorts)
            {
                if (!PortsList.Items.Contains(port))
                    PortsList.Items.Add(port);
            }
            foreach (string port in portListItems)
            {
                if (!availablePorts.Contains(port))
                    PortsList.Items.Remove(port);
            }

            // Select ggbot port if present, otherwise disconnect
            if (PortsList.Items.Contains(ggbotPort))
                PortsList.SelectedIndex = PortsList.Items.IndexOf(ggbotPort);
            else if (geombotState == GeombotState.Connected)
            {
                Disconnect();
                ggbotPort = "";
            }
        }

        public void TestSerialCommand(string cmd)
        {
            if (geombotState == GeombotState.Disconnected)
                return;

            try
            {
                //serialManager.Connect("COM13");
                serialManager.Write(cmd);
                debugLog.Text = serialManager.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }



        //#####################################################
        //GUI EVENTS
        //#####################################################

        private void Connect_Click(object sender, EventArgs e)
        {
            if (geombotState == GeombotState.Connected) return;
            if (ggbotPort == "") return;

            Connect();
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            //notifyIcon1.Visible = false;
        }

        private void GoToGuides(object sender, EventArgs e)
        {
            Process.Start("https://www.percontare.it");
        }

        private void GoToSnap(object sender, EventArgs e)
        {
            Process.Start("https://www.percontare.it/snap/ggbot-snap/");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Test_Click(object sender, EventArgs e)
        {
            TestSerialCommand("[F/15]");
        }

        private void PortsList_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void PortsList_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }

        private void PortsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PortsList.SelectedItem == null) return;

            if (geombotState == GeombotState.Connected)
                Disconnect();

            ggbotPort = PortsList.SelectedItem.ToString();
        }

        void OnTryClose(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                // THIS IS FOR MINIMIZING WINDOW
                WindowState = FormWindowState.Minimized;

                // THIS IS FOR SYSTEM ICON
                //Hide();
                //notifyIcon1.Visible = true;

                e.Cancel = true;
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            ggbotPort = serialManager.AutodetectArduinoPort();
            RefreshPortList();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //if (geombotState == GeombotState.Connected) return;

            ggbotPort = serialManager.AutodetectArduinoPort();
            RefreshPortList();

            debugLog.Text = ggbotPort;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            TestSerialCommand("[C/" + numericUpDown1.Value + "]");
        }
    }


    // TO HAVE A SINGLE INSTANCE APPLICATION
    public static class SingleInstance
    {
        static Mutex mutex;
        public static bool Start(string applicationIdentifier)
        {
            bool isSingleInstance = false;
            mutex = new Mutex(true, applicationIdentifier, out isSingleInstance);
            return isSingleInstance;
        }
        public static void Stop()
        {
            mutex.ReleaseMutex();
        }
    }


    public class HttpServer
    {
        public static HttpListener listener;
        public static string URL = "";
        public static int requestCount = 0;

        public HttpServer(string url)
        {
            URL = url;
            Run();
        }

        public static async Task HandleIncomingConnections()
        {
            bool runServer = true;
            string receivedData = "";

            // While a user hasn't visited the `shutdown` url, keep on handling requests
            while (runServer)
            {
                // Will wait here until we hear from a connection
                HttpListenerContext ctx = await listener.GetContextAsync();

                // Peel out the requests and response objects
                HttpListenerRequest req = ctx.Request;
                HttpListenerResponse resp = ctx.Response;

                // Print out some info about the request
                Console.WriteLine("Request #: {0}", ++requestCount);
                Console.WriteLine(req.Url.ToString());
                Console.WriteLine(req.HttpMethod);
                Console.WriteLine(req.UserHostName);
                Console.WriteLine(req.UserAgent);
                Console.WriteLine();

                string path = req.Url.AbsolutePath;

                //// If `shutdown` url requested w/ POST, then shutdown the server after serving the page
                //if ((req.HttpMethod == "POST") && (path == "/shutdown"))
                //{
                //    Console.WriteLine("Shutdown requested");
                //    runServer = false;
                //}

                if (Form1.geombotState == Form1.GeombotState.Connected && req.HttpMethod == "GET") // Sometimes it gets duplicate requests if I don't filter for GET method
                {
                    // Write on port
                    if (path.Contains("["))
                    {
                        Console.WriteLine("Received command:");
                        string command = path.Split('[')[1];
                        command = "[" + command;
                        Console.WriteLine(command);
                        Form1.serialManager.Write(command);
                    }

                    // Wait for robot OK signal
                    receivedData = "";
                    while (receivedData != "OK")
                    {
                        receivedData = Form1.serialManager.ReadLine();
                        Thread.Sleep(100);
                    }
                }

                // Write the response info
                //string disableSubmit = !runServer ? "disabled" : "";
                //byte[] data = Encoding.UTF8.GetBytes(String.Format(pageData, pageViews, disableSubmit));
                byte[] data = Encoding.UTF8.GetBytes("GGBot: " + receivedData);
                resp.ContentType = "text/html";
                resp.ContentEncoding = Encoding.UTF8;
                resp.ContentLength64 = data.LongLength;

                // Write out to the response stream (asynchronously), then close it
                await resp.OutputStream.WriteAsync(data, 0, data.Length);
                resp.Close();
            }
        }

        public static void Run()
        {
            // Create a Http server and start listening for incoming connections
            listener = new HttpListener();
            listener.Prefixes.Add(URL);
            listener.Start();
            Console.WriteLine("Listening for connections on {0}", URL);

            // Handle requests
            Task listenTask = HandleIncomingConnections();
            listenTask.GetAwaiter().GetResult();

            // Close the listener
            listener.Close();
        }
    }


    public class SerialManager
    {
        //public string detectedGgbotPort = "";
        protected SerialPort serialPort;
        //public bool testingPorts;
        //Thread testConnectThread;
        //bool connected;

        public string[] GetPorts()
        {
            return SerialPort.GetPortNames();
        }

        //public void AutoDetectGgbotPort()
        //{
        //    testingPorts = true;
        //    testConnectThread = new Thread(TestAllPorts);
        //    testConnectThread.Start();
        //}

        //void TestAllPorts()
        //{
        //    string[] ports = GetPorts();
        //    foreach (string port in ports)
        //    {
        //        TestPort(port);                
        //    }
        //    testingPorts = false;
        //}

        //void TestPort(string portName)
        //{
        //    Connect(portName);
        //    Write("[F/1]");

        //    Thread.Sleep(3000);

        //    string response = ReadLine();
        //    if (response == "OK")
        //        detectedGgbotPort = portName;
        //    Disconnect();
        //}

        public string AutodetectArduinoPort()
        {
            //ManagementScope connectionScope = new ManagementScope();
            //SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'");

            ////using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'"))
            ////{
            ////    var portnames = SerialPort.GetPortNames();
            ////    var ports = searcher.Get().Cast<ManagementBaseObject>().ToList().Select(p => p["Caption"].ToString());

            ////    var portList = portnames.Select(n => n + " - " + ports.FirstOrDefault(s => s.Contains(n))).ToList();

            ////    foreach (string s in portList)
            ////    {
            ////        Console.WriteLine(s);
            ////    }
            ////    return portnames[0];
            ////}



            //ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

            ////try
            ////{
            //Console.WriteLine("TEST");
            //foreach (ManagementObject item in searcher.Get())
            //{
            //    foreach (PropertyData p in item.Properties)
            //        Console.WriteLine(p.Name + ": " + p.Value);
            //    string desc = item["Manufacturer"].ToString();
            //    //string deviceId = item["DeviceID"].ToString();
            //    //Console.WriteLine(desc);
            //    //Console.WriteLine(deviceId);

            //    if (desc.Contains("FTDI"))
            //    {
            //        //return item;
            //    }
            //}
            ////}
            ////catch (ManagementException e)
            ////{
            ////    /* Do Nothing */
            ////}
            ///


            using (ManagementClass i_Entity = new ManagementClass("Win32_PnPEntity"))
            {
                foreach (ManagementObject i_Inst in i_Entity.GetInstances())
                {
                    Object o_Guid = i_Inst.GetPropertyValue("ClassGuid");
                    if (o_Guid == null || o_Guid.ToString().ToUpper() != "{4D36E978-E325-11CE-BFC1-08002BE10318}")
                        continue; // Skip all devices except device class "PORTS"

                    String s_Caption = i_Inst.GetPropertyValue("Caption").ToString();
                    String s_Manufact = i_Inst.GetPropertyValue("Manufacturer").ToString();
                    String s_DeviceID = i_Inst.GetPropertyValue("PnpDeviceID").ToString();
                    String s_RegPath = "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Enum\\" + s_DeviceID + "\\Device Parameters";
                    String s_PortName = Registry.GetValue(s_RegPath, "PortName", "").ToString();

                    int s32_Pos = s_Caption.IndexOf(" (COM");
                    if (s32_Pos > 0) // remove COM port from description
                        s_Caption = s_Caption.Substring(0, s32_Pos);

                    Console.WriteLine("Port Name:    " + s_PortName);
                    Console.WriteLine("Description:  " + s_Caption);
                    Console.WriteLine("Manufacturer: " + s_Manufact);
                    Console.WriteLine("Device ID:    " + s_DeviceID);
                    Console.WriteLine("-----------------------------------");


                    // Got GGBot Port
                    if (s_Manufact == "FTDI") return s_PortName;
                }
            }

            return "";
        }

        public void Connect(string port)
        {
            serialPort = new SerialPort();
            serialPort.PortName = port;//Set your board COM
            serialPort.BaudRate = 9600;
            serialPort.Open();
        }

        public void Write(string text)
        {
            serialPort.Write(text);
        }

        public string ReadLine()
        {
            string response = serialPort.ReadLine();
            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();
            return response.Trim();
        }

        public void Disconnect()
        {
            serialPort.Close();
            serialPort = null;
        }
    }
}
