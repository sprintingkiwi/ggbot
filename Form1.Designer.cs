﻿
namespace GGBot
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.Snap = new System.Windows.Forms.Button();
            this.Guide = new System.Windows.Forms.Button();
            this.Quit = new System.Windows.Forms.Button();
            this.Connect_Btn = new System.Windows.Forms.Button();
            this.Test = new System.Windows.Forms.Button();
            this.PortsList = new System.Windows.Forms.ListBox();
            this.Refresh = new System.Windows.Forms.Button();
            this.Logo = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.debugLog = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // Snap
            // 
            this.Snap.BackColor = System.Drawing.SystemColors.Info;
            this.Snap.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Snap.Location = new System.Drawing.Point(477, 230);
            this.Snap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Snap.Name = "Snap";
            this.Snap.Size = new System.Drawing.Size(183, 86);
            this.Snap.TabIndex = 1;
            this.Snap.Text = "Programma con SNAP!";
            this.Snap.UseVisualStyleBackColor = false;
            this.Snap.Click += new System.EventHandler(this.GoToSnap);
            // 
            // Guide
            // 
            this.Guide.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Guide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guide.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Guide.Location = new System.Drawing.Point(12, 361);
            this.Guide.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Guide.Name = "Guide";
            this.Guide.Size = new System.Drawing.Size(183, 81);
            this.Guide.TabIndex = 2;
            this.Guide.Text = "Vai alle guide";
            this.Guide.UseVisualStyleBackColor = false;
            this.Guide.Click += new System.EventHandler(this.GoToGuides);
            // 
            // Quit
            // 
            this.Quit.BackColor = System.Drawing.Color.LightCoral;
            this.Quit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Quit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quit.Location = new System.Drawing.Point(477, 361);
            this.Quit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Quit.Name = "Quit";
            this.Quit.Size = new System.Drawing.Size(183, 81);
            this.Quit.TabIndex = 3;
            this.Quit.Text = "DISCONNETTI ED ESCI";
            this.Quit.UseVisualStyleBackColor = false;
            this.Quit.Click += new System.EventHandler(this.CloseApplication);
            // 
            // Connect_Btn
            // 
            this.Connect_Btn.BackColor = System.Drawing.Color.NavajoWhite;
            this.Connect_Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Connect_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Connect_Btn.Location = new System.Drawing.Point(453, 12);
            this.Connect_Btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Connect_Btn.Name = "Connect_Btn";
            this.Connect_Btn.Size = new System.Drawing.Size(207, 103);
            this.Connect_Btn.TabIndex = 0;
            this.Connect_Btn.Text = "Connetti GGBot";
            this.Connect_Btn.UseVisualStyleBackColor = false;
            this.Connect_Btn.Click += new System.EventHandler(this.Connect_Click);
            // 
            // Test
            // 
            this.Test.Location = new System.Drawing.Point(587, 128);
            this.Test.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Test.Name = "Test";
            this.Test.Size = new System.Drawing.Size(75, 23);
            this.Test.TabIndex = 5;
            this.Test.Text = "Test";
            this.Test.UseVisualStyleBackColor = true;
            this.Test.Click += new System.EventHandler(this.Test_Click);
            // 
            // PortsList
            // 
            this.PortsList.FormattingEnabled = true;
            this.PortsList.ItemHeight = 16;
            this.PortsList.Location = new System.Drawing.Point(453, 128);
            this.PortsList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PortsList.Name = "PortsList";
            this.PortsList.Size = new System.Drawing.Size(120, 68);
            this.PortsList.TabIndex = 7;
            this.PortsList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PortsList_MouseClick);
            this.PortsList.SelectedIndexChanged += new System.EventHandler(this.PortsList_SelectedIndexChanged);
            this.PortsList.SelectedValueChanged += new System.EventHandler(this.PortsList_SelectedValueChanged);
            // 
            // Refresh
            // 
            this.Refresh.Image = global::GGBot.Properties.Resources.Refresh;
            this.Refresh.Location = new System.Drawing.Point(571, 158);
            this.Refresh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Refresh.Name = "Refresh";
            this.Refresh.Size = new System.Drawing.Size(35, 39);
            this.Refresh.TabIndex = 8;
            this.Refresh.UseVisualStyleBackColor = true;
            this.Refresh.Visible = false;
            this.Refresh.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // Logo
            // 
            this.Logo.BackgroundImage = global::GGBot.Properties.Resources.loghi;
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Logo.Location = new System.Drawing.Point(12, 12);
            this.Logo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(423, 304);
            this.Logo.TabIndex = 4;
            this.Logo.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // debugLog
            // 
            this.debugLog.Location = new System.Drawing.Point(276, 331);
            this.debugLog.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.debugLog.Name = "debugLog";
            this.debugLog.Size = new System.Drawing.Size(100, 22);
            this.debugLog.TabIndex = 9;
            this.debugLog.Visible = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numericUpDown1.Location = new System.Drawing.Point(276, 380);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown1.TabIndex = 10;
            this.numericUpDown1.Value = new decimal(new int[] {
            180,
            0,
            0,
            65536});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(276, 359);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Calibrazione rotazione";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(673, 455);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.debugLog);
            this.Controls.Add(this.Refresh);
            this.Controls.Add(this.PortsList);
            this.Controls.Add(this.Test);
            this.Controls.Add(this.Logo);
            this.Controls.Add(this.Quit);
            this.Controls.Add(this.Guide);
            this.Controls.Add(this.Snap);
            this.Controls.Add(this.Connect_Btn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "GGBot";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnTryClose);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Connect_Btn;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button Snap;
        private System.Windows.Forms.Button Guide;
        private System.Windows.Forms.Button Quit;
        private System.Windows.Forms.Panel Logo;
        private System.Windows.Forms.Button Test;
        private System.Windows.Forms.ListBox PortsList;
        private System.Windows.Forms.Button Refresh;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox debugLog;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
    }
}

